package auth

import (
	"github.com/ant0ine/go-json-rest"
	"log"
	"net/http"
)

//Rest api
type AuthRestService struct {
	ggleConfig, facebookConfig *OAuthConfig
	AuthSerivce                *AuthService
}

type OAuthConfig struct {
	ClientId, ClientSecret, Scope, TokenUrl, RedirectUrl, UserProfileUrl string
}

func (r *AuthRestService) fbConfig(fbConfig *OAuthConfig) {
	r.facebookConfig = fbConfig
}
func (r *AuthRestService) googleConfig(googleConfig *OAuthConfig) {
	r.ggleConfig = googleConfig
}

func (r *AuthRestService) Init(fbConfig *OAuthConfig, googleConfig *OAuthConfig) {
	log.Println("AuthRestService:Init")
	r.fbConfig(fbConfig)
	r.googleConfig(googleConfig)

	handler := rest.ResourceHandler{
		EnableLogAsJson:          true,
		EnableRelaxedContentType: true,
		EnableResponseStackTrace: true,
		EnableStatusService:      true}

	handler.SetRoutes(
		rest.Route{"GET", "/api/auth/callback", r.OAuthCallback},
		rest.Route{"POST", "/api/auth/login", r.Login},
		rest.Route{"POST", "/api/auth/register", r.Register},
		rest.Route{"GET", "/api/auth/profiles/:id", r.AuthSerivce.AuthHandler(r.GetProfile)},
		rest.Route{"PUT", "/api/auth/profiles/:id", r.AuthSerivce.AuthHandler(r.UpdateProfile)},
	)

	http.Handle("/api/auth/", &handler)
}

func (this *AuthRestService) OAuthCallback(w *rest.ResponseWriter, r *rest.Request) {
	err := r.ParseForm()
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	code := r.FormValue("code")
	state := r.FormValue("state")
	var authProvider string

	oauth := new(OAuth)
	var conf *OAuthConfig
	if state == "fb" {
		conf = this.facebookConfig
		authProvider = "facebook"
	} else {
		conf = this.ggleConfig
		authProvider = "google"
	}
	token, err := oauth.ObtainAccessToken(conf.TokenUrl, code, conf.RedirectUrl, conf.Scope, conf.ClientId, conf.ClientSecret)

	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userInfo := &struct{ Id, DisplayName, FullName string }{}
	if authProvider == "facebook" {
		fbUserInfo := &struct {
			Id         string
			Name       string
			First_name string
			Last_name  string
		}{}

		oauth.ObtainUserInfo(conf.UserProfileUrl, token, fbUserInfo)
		userInfo.Id = fbUserInfo.Id
		userInfo.DisplayName = fbUserInfo.Name
		userInfo.FullName = fbUserInfo.First_name + " " + fbUserInfo.Last_name
	} else {
		oauth.ObtainUserInfo(conf.UserProfileUrl, token, userInfo)
	}

	var userId int
	user := this.AuthSerivce.GetOAuthUser(userInfo.Id, authProvider)
	if user == nil {
		userId = this.AuthSerivce.AddOAuthUser(userInfo.Id, userInfo.DisplayName, userInfo.FullName, authProvider)
	} else {
		userId = user.Id
	}
	this.AuthSerivce.OAuthenticate(userId, token, authProvider)

	//in case of user cancelling access
	// /api/auth/callback?error=access_denied&error_code=200
	// &error_description=Permissions+error&error_reason=user_denied

	emberjsSuccess := "<html><head><script>window.opener.Ember.SimpleAuth.externalLoginSucceeded({ access_token: '" + token + "' });window.close();</script></head></html>"
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(emberjsSuccess))
}

func (this *AuthRestService) Register(w *rest.ResponseWriter, r *rest.Request) {
	register := struct{ Email, Password string }{}
	err := r.DecodeJsonPayload(&register)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = this.AuthSerivce.AddUser(register.Email, register.Password)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (this *AuthRestService) Login(w *rest.ResponseWriter, r *rest.Request) {
	login := struct{ Email, Password string }{}
	err := r.DecodeJsonPayload(&login)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	token, err := this.AuthSerivce.Authenticate(login.Email, login.Password)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	respo := struct {
		AccessToken string `json:access_token`
	}{token}
	w.WriteJson(respo)
}

func (this *AuthRestService) GetProfile(w *rest.ResponseWriter, r *rest.Request, userId int) {
	log.Println("GetProfile")
	uRepo := *this.AuthSerivce.userRepo
	user, _ := uRepo.FindById(userId)
	profile := struct {
		Id          int    `json:"id"`
		Email       string `json:"email"`
		DisplayName string `json:"displayName"`
		FullName    string `json:"fullName"`
		HasFacebook bool   `json:"hasFacebook"`
		HasGoogle   bool   `json:"hasGoogle"`
	}{userId, user.Email, user.DisplayName, user.FullName, user.FacebookId != "", user.GoogleId != ""}

	w.WriteJson(map[string]interface{}{"profile": profile})
}

type updateProfile struct {
		Email       string
		DisplayName string
		FullName    string

}
func (this *AuthRestService) UpdateProfile(w *rest.ResponseWriter, r *rest.Request, userId int) {
	log.Println("UpdateProfile")
	updReq := struct {
		Profile User
	}{}
	err := r.DecodeJsonPayload(&updReq)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Printf("user updated: %+v", updReq)
	
	uRepo := *this.AuthSerivce.userRepo
	user, _ := uRepo.FindById(userId)
	user.DisplayName = updReq.Profile.DisplayName
	user.Email = updReq.Profile.Email
	user.FullName = updReq.Profile.FullName
	
	log.Printf("user updated: %+v", user)
	uRepo.Store(user)
}
