package auth

type UserRepository interface {
	AddOAuthUser(oAuthId, displayName, fullName, state string) (id int)
	FindAll() ([]User)
	FindByEmail(email string) (*User, bool)
	FindById(id int) (*User, bool)
	FindByOAuthId(oAuthId string, oauthProvider string) *User
	Store(user *User)
}

type SessionRepository interface {
	Store(session *Session)
	Find(token string) (*Session, bool)
	FindAll() ([]Session)
}

type User struct {
	Id int
	Email string
	Password string
	
	FacebookId string
	GoogleId string
	DisplayName string
	FullName string
}

type Session struct {
	Id int
	UserId int
	AuthToken string
	Source string
}