package auth

import (
	"code.google.com/p/go.crypto/bcrypt"
	"github.com/ant0ine/go-json-rest"
	"crypto/rand"
	"crypto/sha1"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

type AuthService struct {
	userRepo    *UserRepository
	sessionRepo *SessionRepository
}

func CreateAuthService(ur *UserRepository, sr *SessionRepository) *AuthService {
	service := &AuthService{ur, sr}
	return service
}

func (this *AuthService) AddUser(email, passwd string) error {
	if !isEmail(email) {
		return errors.New("error.user.invalidEmail")
	}

	uRepo := *this.userRepo
	_, f := uRepo.FindByEmail(email)
	if f {
		return errors.New("error.user.emailInUse")
	}
	user := &User{}
	user.Email = email
	user.Password = hashPasswd(passwd)
	uRepo.Store(user)
	return nil
}

func (this *AuthService) Authenticate(email, passwd string) (string, error) {
	u, f := (*this.userRepo).FindByEmail(email)

	if f && u.Password == hashPasswd(passwd) {
		token := generateAuthToken()
		session := Session{}
		session.AuthToken = token
		session.Source = "local"
		session.UserId = u.Id
		(*this.sessionRepo).Store(&session)
		return token, nil
	} else {
		log.Println("Unable to authenticate, invalid password: ", email)
	}
	return "", errors.New("error.user.invalidEmailOrPasswd")
}

func (this *AuthService) OAuthenticate(userId int, authToken, source string) {
	session := Session{}
	session.AuthToken = authToken
	session.Source = source
	session.UserId = userId
	(*this.sessionRepo).Store(&session)
}

func (this *AuthService) GetAuthenticatedUser(token string) (userId int, found bool) {
	ses, found := (*this.sessionRepo).Find(token)
	if found {
		return ses.UserId, true
	}
	return 0, false
}

func (this *AuthService) GetOAuthUser(authUserId, oauthProvider string) *User {
	return (*this.userRepo).FindByOAuthId(authUserId, oauthProvider)
}

func (this *AuthService) AddOAuthUser(oauthId, displayName, fullName, oauthProvider string) int {
	u := User{}
	if oauthProvider == "facebook" {
		u.FacebookId = oauthId
	} else if oauthProvider == "google" {
		u.GoogleId = oauthId
	}
	u.DisplayName = displayName
	u.FullName = fullName
	(*this.userRepo).Store(&u)
	return u.Id
}


func (this *AuthService) AuthHandler(fn func(*rest.ResponseWriter, *rest.Request, int)) func(w *rest.ResponseWriter, r *rest.Request) {
	return func(w *rest.ResponseWriter, r *rest.Request) {
		log.Println("AuthHandler:request")
		token := r.Header.Get("auth_token")
		if token == "" {
			token = r.Header.Get("Authorization")
			token = token[7:len(token)]
		}

		auth := false
		if token != "" {
			userId, found := this.GetAuthenticatedUser(token)
			if found {
				fn(w, r, userId)
				return
			}
		}
		if !auth {
			http.Error(w, "error.notAuthenticated", http.StatusUnauthorized)
			return
		}
	}
}


func hashPasswd(plain string) string {
	hasher := sha512.New()
	hasher.Write([]byte(plain))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha
}

// newUUID generates a random UUID according to RFC 4122
func generateAuthToken() string {
	uuid := make([]byte, 16)
	io.ReadFull(rand.Reader, uuid)
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	token := fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
	tokenByte, err := bcrypt.GenerateFromPassword([]byte(token), bcrypt.DefaultCost)
	if err != nil {
		panic(err) //this is a panic because bcrypt errors on invalid costs
	}
	h := sha1.New()
	h.Write(tokenByte)
	h.Sum(nil)
	return string(tokenByte)
}

func isEmail(e string) bool {
	return len(e) >= 5 && strings.Contains(e, "@")
}
