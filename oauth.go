package auth

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)


type OAuth struct {
	transport http.RoundTripper
}

var transport http.RoundTripper

var UseSSL bool = true

type AccessToken struct {
	Access    string        `json:"access_token"`
	Refresh   string        `json:"refresh_token"`
	ExpiresIn time.Duration `json:"expires_in"`
}

func (this *OAuth) getTransport() http.RoundTripper {
	if this.transport != nil {
		return this.transport
	}
	return http.DefaultTransport
}

func (this *OAuth) SetTransport(t http.RoundTripper) {
	this.transport = t
}

func (this *OAuth)  ObtainAccessToken(authUrl, code, redirectUri, scope, clientId, clientSecret string) (string, error) {
	client := &http.Client{Transport: this.getTransport()}

	data := url.Values{
		"code":          {code},
		"redirect_uri":  {redirectUri},
		"grant_type":    {"authorization_code"},
		"scope":         {scope},
		"client_id":     {clientId},
		"client_secret": {clientSecret},
	}

	request, err := http.NewRequest("POST", authUrl, strings.NewReader(data.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// make the request.
	response, err := client.Do(request)

	if err != nil {
		log.Printf("ObtainAccessToken request: error: %+v", err)
		return "", err
	}
	if response.StatusCode != 200 {
		log.Printf("error response from oauth provider was: %+v", response)
		return "", errors.New("OAuth provider returned error")
	}

	token := new(AccessToken)
	if strings.Contains(authUrl, "facebook.com") {
		err = readFacebookToken(response, token)
	} else {
		err = parseJSONResponse(response, token)
	}
	return token.Access, err
}

func (this *OAuth) ObtainUserInfo(profileUrl, authToken string, userInfo interface{}) error {
	client := &http.Client{Transport: this.getTransport()}

	request, err := http.NewRequest("GET", profileUrl, strings.NewReader(""))
	if err != nil {
		return err
	}
	request.Header.Set("Authorization", "OAuth "+authToken)

	response, err := client.Do(request)
	if err != nil {
		return err
	}

	return parseJSONResponse(response, userInfo)
}

func readBody(response *http.Response) ([]byte, error) {
	defer response.Body.Close()
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode >= 400 && response.StatusCode <= 500 {
		err = errors.New("Bad Request")
		return nil, err
	}
	return bytes, nil
}

func readFacebookToken(response *http.Response, token *AccessToken) error {
	body, err := readBody(response)
	if err != nil {
		return err
	}
	vals, err := url.ParseQuery(string(body))
	if err != nil {
		return err
	}
	token.Access = vals.Get("access_token")
	expires_in, err := strconv.ParseInt(vals.Get("expires"), 10, 64)
	if err != nil {
		return err
	}
	token.ExpiresIn = time.Duration(expires_in)

	return nil
}

// parse the response
func parseJSONResponse(response *http.Response, result interface{}) (error error) {
	defer response.Body.Close()
	bytes, error := ioutil.ReadAll(response.Body)
	if error != nil {
		return
	}

	if response.StatusCode >= 400 && response.StatusCode <= 500 {
		error = errors.New("Bad Request")
		return
	}
	
	//parse JSON
	error = json.Unmarshal(bytes, result)
	if error != nil {
		print(error.Error())
		return error
	}

	return
}
