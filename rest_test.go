package auth

import (
	"encoding/json"
	"github.com/ant0ine/go-json-rest"
	"github.com/ant0ine/go-json-rest/test"
	//"log"
	"strings"
	"testing"
)

func TestDecodeFb(t *testing.T) {
	jsonTxt := `{"id":"100000304611075",
			  "name":"Tomek Szpinda",
			  "first_name":"Tomek",
			  "username":"tomek.szpinda",
			  "email":"tszpinda\u0040hotmail.com",
			  "updated_time":"2013-01-31T16:51:51+0000"}`

	contentJson := strings.Replace(jsonTxt, "\\u0040", "@", 1)
	if !strings.Contains(contentJson, "@") {
		t.Fatal("email not replaced", contentJson)
	}

}

var authRestService *AuthRestService


func TestSetupTest(t *testing.T) {
	setup()
	if authRestService == nil {
		t.Fatal("authRestService == nil")
	}
	srv := authRestService.AuthSerivce
	if srv == nil {
		t.Fatal("srv == nil")
	}
	srv.AddUser("t@gm.com", "ad1234")
}
func TestRegisterWithLogin(t *testing.T) {
	setup()
	handler := rest.ResourceHandler{
		DisableJsonIndent: true,
	}
	handler.SetRoutes(rest.Route{"POST", "/register", authRestService.Register})
	register := test.RunRequest(t, &handler, test.MakeSimpleRequest("POST", "http://1.2.3.4/register", &map[string]string{"Email": "tom@gmail.com", "Password": "Soon"}))
	register.CodeIs(200)

	handler.SetRoutes(rest.Route{"POST", "/login", authRestService.Login})
	login := test.RunRequest(t, &handler, test.MakeSimpleRequest("POST", "http://1.2.3.4/login", &map[string]string{"Email": "tom@gmail.com", "Password": "Soon"}))
	login.CodeIs(200)

	loginResponse := struct {
		AccessToken string `json:access_token`
	}{}
	err := json.Unmarshal(login.Recorder.Body.Bytes(), &loginResponse)
	if err != nil {
		t.Fatalf("invalid response: %+v", err)
	}

	if loginResponse.AccessToken == "" {
		t.Fatal("invalid token", loginResponse.AccessToken)
	}
}

func TestGetProfile(t *testing.T) {
	setup()
	authRestService.AuthSerivce.AddUser("bill@gmail.com", "Passwd1")
	token, _ := authRestService.AuthSerivce.Authenticate("bill@gmail.com", "Passwd1")
	
	
	handler := rest.ResourceHandler{
		DisableJsonIndent: true,
	}
	handler.SetRoutes(rest.Route{"GET", "/profiles/:id", authRestService.AuthSerivce.AuthHandler(authRestService.GetProfile)})
	test.MakeSimpleRequest("GET", "http://1.2.3.4/profiles/:id", "auth_token:"+token)
	
	profileReq := test.MakeSimpleRequest("GET", "http://1.2.3.4/profiles/0", nil)
	profileReq.Header.Set("Content-Type", "application/json")
	profileReq.Header.Set("auth_token", token)
	response := test.RunRequest(t, &handler, profileReq)
	response.CodeIs(200)
	response.BodyIs(`{"profile":{"id":1,"email":"bill@gmail.com","displayName":"","fullName":"","hasFacebook":false,"hasGoogle":false}}`)
}

func TestUpdateProfile(t *testing.T) {
	setup()
	authRestService.AuthSerivce.AddUser("bill@gmail.com", "Passwd1")
	token, _ := authRestService.AuthSerivce.Authenticate("bill@gmail.com", "Passwd1")
	
	
	handler := rest.ResourceHandler{
		DisableJsonIndent: true,
	}
	handler.SetRoutes(rest.Route{"PUT", "/profiles/:id", authRestService.AuthSerivce.AuthHandler(authRestService.UpdateProfile)})
	test.MakeSimpleRequest("PUT", "http://1.2.3.4/profiles/:id", "auth_token:"+token)
	
	content := &map[string]string{"email": "tom@gmail.com", "displayName": "Nick", "FullName": "Bob Nick"}
	wrapper := &map[string]interface{}{"profile": content}
	
	profileReq := test.MakeSimpleRequest("PUT", "http://1.2.3.4/profiles/0", wrapper)
	profileReq.Header.Set("Content-Type", "application/json")
	profileReq.Header.Set("auth_token", token)
	response := test.RunRequest(t, &handler, profileReq)
	response.CodeIs(200)
	uRepo := *authRestService.AuthSerivce.userRepo
	u, f := uRepo.FindByEmail("tom@gmail.com")
	if !f {
		t.Fatal("unable to find user with email: tom@gmail.com")
	}
	if u.Email != "tom@gmail.com" {
		t.Fatal("email not updated was:", u.Email)
	}
	if u.DisplayName != "Nick" {
		t.Fatal("displayName not updated was:", u.DisplayName)
	}
	if u.FullName != "Bob Nick" {
		t.Fatal("fullName not updated was:", u.FullName)
	}
}


func setup() {
	var userRepo UserRepository = new(InMemoryUserRepository)
	var sessionRepo SessionRepository = new(InMemorySessionRepository)
	authSerivce := CreateAuthService(&userRepo, &sessionRepo)
	authRestService = new(AuthRestService)
	authRestService.AuthSerivce = authSerivce
}