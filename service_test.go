package auth

import (
	"testing"
)
var ur UserRepository
var sr SessionRepository
func getService() *AuthService {
	ur = new(InMemoryUserRepository)
 	sr = new(InMemorySessionRepository)
	return &AuthService{&ur, &sr}
}

func TestGenerateToken(t *testing.T) {
	token := generateAuthToken()
	if token == "" {
		t.Fatal("token was empty")
	}
	token2 := generateAuthToken()
	if token == token2 {
		t.Fatal("duplicated tokens")
	}
}

func TestIsEmail(t *testing.T) {
	if !isEmail("t@c.c") {
		t.Fatal("should be valid")
	}
	if !isEmail("tomek@hot.com") {
		t.Fatal("should be valid")
	}
	if isEmail("tomekhot.com") {
		t.Fatal("should not be valid")
	}
	if isEmail("@.om") {
		t.Fatal("should not be valid")
	}
}

func TestAddUser(t *testing.T) {
	service := getService();
	service.AddUser("tom@gmail.com", "test1234")
	if len(ur.FindAll()) != 1 {
		t.Fatalf("expected one user was: ", len(ur.FindAll()))
	}
	addedUser := ur.FindAll()[0]
	if addedUser.Email != "tom@gmail.com" {
		t.Fatalf("expected diff user email, was: %v", addedUser)
	}
	
	err := service.AddUser("tom@gmail.com", "test1234")
	if err == nil {
		t.Fatal("attemt to add user with the same email should fail")
	}
	
	err = service.AddUser("tomgmail.com", "test1234")
	if err == nil {
		t.Fatal("attemt to add user with invalid email should fail")
	}
}

func TestAuthenticate(t *testing.T) {
	service := getService();
	service.AddUser("tom@gmail.com", "test1234")
	
	token, err := service.Authenticate("tom@gmail.com", "test1234")
	if err != nil {
		t.Fatalf("unexpected error: %+v", err)
	}
	if token == "" {
		t.Fatalf("expected not empty auth token")
	}
	
	if len(sr.FindAll()) != 1 {
		t.Fatalf("expected one session was", len(sr.FindAll()))
	}
	
	_, err = service.Authenticate("tom@gmail.com", "test12345")
	if err == nil {
		t.Fatal("should not be able to authenticate with invalid password")
	}
	if err.Error() != "error.user.invalidEmailOrPasswd" {
		t.Fatalf("invalid error: %v", err.Error())
	}
	_, err = service.Authenticate("tom@gmail.co", "test1234")
	if err == nil {
		t.Fatal("should not be able to authenticate with invalid user")
	}
	if err.Error() != "error.user.invalidEmailOrPasswd" {
		t.Fatalf("invalid error: %v", err.Error())
	}
}

func TestOAuthenticate(t *testing.T) {
	service := getService()
	service.OAuthenticate(23, "token-ausx", "google")
	if len(sr.FindAll()) != 1 {
		t.Fatal("expected only one session")
	}
}

func TestGetAuthenticatedUser(t *testing.T) {
	service := getService()
	service.OAuthenticate(23, "token-1234", "google")
	id, f := service.GetAuthenticatedUser("token-1234")
	if !f {
		t.Fatal("user not found")
	}
	if id != 23 {
		t.Fatal("invalid id")
	}
	
	_, f = service.GetAuthenticatedUser("token-12345")
	if f {
		t.Fatal("user found")
	}
}

func TestGetOAuthUser(t *testing.T) {
	service := getService();
	u := service.GetOAuthUser("12", "google")
	if u != nil {
		t.Fatalf("expected nil was: %+v", u)
	}
	service.AddOAuthUser("12", "tomek", "bill bonk", "google")
	u = service.GetOAuthUser("12", "google")
	if u == nil {
		t.Fatal("user not found")
	}
}

func TestAddOAuthUser(t *testing.T) {
	service := getService();
	service.AddOAuthUser("12", "tomek", "bill bonk", "google")
	u := service.GetOAuthUser("12", "google")
	if u == nil {
		t.Fatal("user not found")
	}
	if u.DisplayName != "tomek" {
		t.Fatalf("expected tomek was: %+v", u)
	}

}
