package auth

type InMemoryUserRepository struct {
	users []User
}

func (r *InMemoryUserRepository) AddOAuthUser(oauthId, displayName, fullName, oauthProvider string) int {
	u := User{}
	if oauthProvider == "facebook" {
		u.FacebookId = oauthId
	} else if oauthProvider == "google" {
		u.GoogleId = oauthId
	}
	u.DisplayName = displayName
	u.FullName = fullName
	u.Id = len(r.users) + 1
	r.users = append(r.users, u)
	return u.Id
}

func (r *InMemoryUserRepository) FindById(id int) (*User, bool) {
	for _, u := range r.users {
		if u.Id == id {
			return &u, true
		}
	}
	return nil, false
}
func (r *InMemoryUserRepository) FindByEmail(email string) (*User, bool) {
	for _, u := range r.users {
		if u.Email == email {
			return &u, true
		}
	}
	return nil, false
}

func (r *InMemoryUserRepository) FindByOAuthId(authUserId, oauthProvider string) *User {
	for _, u := range r.users {
		if oauthProvider == "facebook" && authUserId == u.FacebookId {
			return &u
		} else if oauthProvider == "google" && authUserId == u.GoogleId {
			return &u
		}
	}
	return nil
}
func (r *InMemoryUserRepository) FindAll() []User {
	return r.users
}
func (r *InMemoryUserRepository) Store(user *User) {
	if user.Id == 0 {
		user.Id = len(r.users) + 1
		r.users = append(r.users, *user)
	} else {
		index := -1
		for i, u := range r.users {
			if u.Id == user.Id {
				index = i
				break
			}
		}
		if index > -1 {
			r.users[index] = *user
		}
	}
}

//session repo

type InMemorySessionRepository struct {
	sessions []Session
}

func (r *InMemorySessionRepository) Store(session *Session) {
	session.Id = len(r.sessions) + 1
	r.sessions = append(r.sessions, *session)
}
func (r *InMemorySessionRepository) Find(token string) (*Session, bool) {
	for _, s := range r.sessions {
		if s.AuthToken == token {
			return &s, true
		}
	}
	return nil, false
}

func (r *InMemorySessionRepository) FindAll() []Session {
	return r.sessions
}
