package auth

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os/exec"
	"testing"
)

var oauthTest *OAuth

func oauthTestSetup() {
	oauthTest = new(OAuth)
}

func TestObtainAccessToken(t *testing.T) {
	oauthTestSetup()

	authPath := "/graph.facebook.com/oauth/access_token"
	dummyServer := returnDummyResponseForPath(authPath, dummyAccessTokenFacebookResponse, t)
	defer dummyServer.Close()
	authUrl := dummyServer.URL + authPath

	token, err := oauthTest.ObtainAccessToken(authUrl, "abc", "http://localhost:8080/auth/callback", "email", "secret", "def")

	if err != nil {
		t.Fatalf("Error: %+v", err)
	}

	if token != "CAAIoGV3GFYQBAPqSZAgE85E1OkmPslPcba" {
		t.Fatalf("Expected diff token was: %v", token)
	}
}

func TestObtainUserInfo(t *testing.T) {
	oauthTestSetup()

	profilePath := "/graph.facebook.com/me"
	dummyServer := returnDummyResponseForPath(profilePath, dummyUserInfoFacebookResponse, t)
	defer dummyServer.Close()
	profileUrl := dummyServer.URL + profilePath

	userInfo := struct{ Id string }{}
	oauthTest.ObtainUserInfo(profileUrl, "rasdjfjf93485", &userInfo)

	if userInfo.Id != "100000304611015" {
		t.Fatal("expected diff id was:", userInfo.Id)
	}
}

func DisabledTestLiveOAuth(t *testing.T) {
	oauthTestSetup()
	//facebook
	/*
		ClientId := "607039365977476"
		ClientSecret := "d4fb9343c52dbd0264a9e2e9ca8f4e4e"
		Scope := "email"
		TokenURL := "https://graph.facebook.com/oauth/access_token"
		RedirectURL := "http://localhost:8080/api/auth/callback"
		ProfileUrl := "https://graph.facebook.com/me"
		StartUrl := "https://www.facebook.com/dialog/oauth"
	*/
	//google
	ClientId := "477757730579-59qcjnjratpvfdng6nft87hj1e99bm3n.apps.googleusercontent.com"
	ClientSecret := "ChdaxU5XMRgZWcZ7ZWYelwe-"
	Scope := "email profile"
	TokenUrl := "https://accounts.google.com/o/oauth2/token"
	RedirectUrl := "http://localhost:8080/api/auth/callback"
	ProfileUrl := "https://www.googleapis.com/userinfo/v2/me"
	Code := "XRFC-prevent-code"
	
	StartUrl := "https://accounts.google.com/o/oauth2/auth"

	// Get Auth url with scope read and CSRF state set.
	authUrl, _ := url.Parse(StartUrl)
	authQuery := authUrl.Query()
	authQuery.Add("client_id", ClientId)
	authQuery.Add("redirect_uri", RedirectUrl)
	authQuery.Add("response_type", "code")
	authQuery.Add("scope", Scope)
	authUrl.RawQuery = authQuery.Encode()

	// Open browser for authorization.
	// NOTE: This is only for OSX. On linux you should launch a specific
	// browser. E.g. Firefox or chromium etc.
	cmd := exec.Command("open", authUrl.String())
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}

	// Create tcp listener.
	lis, e := net.Listen("tcp", ":8080")
	if e != nil {
		panic(e)
	}

	// Handle response and close connection.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		values := r.URL.Query()

		if values.Get("error") != "" {
			fmt.Fprintf(w, "Error: %v\nDescription: %v", values.Get("error"), values.Get("error_description"))
			defer lis.Close()
			return
		}

		fmt.Fprintf(w, "You logged in successfully. You can now close your browser.")
		log.Printf("Logged %+v", values)
		Code = values.Get("code")

		token, err := oauthTest.ObtainAccessToken(TokenUrl, Code, RedirectUrl, Scope, ClientId, ClientSecret)
		if err != nil {
			panic(err)
		}
		userInfo := struct{ Id string }{}
		oauthTest.ObtainUserInfo(ProfileUrl, token, &userInfo)

		log.Printf("User info: %+v", userInfo)

		defer lis.Close()
	})

	// Start serving http.
	http.Serve(lis, nil)
}

func closeDummyServer(dummyServer *httptest.Server) {
	transport = nil
	dummyServer.Close()
}

func createDummyServer(handler func(w http.ResponseWriter, r *http.Request)) *httptest.Server {
	dummyServer := httptest.NewServer(http.HandlerFunc(handler))

	//change the host to use the test server
	oauthTest.SetTransport(&http.Transport{Proxy: func(*http.Request) (*url.URL, error) { return url.Parse(dummyServer.URL) }})

	//turn off SSL
	UseSSL = false

	return dummyServer
}

func returnDummyResponseForPath(path string, dummy_response string, t *testing.T) *httptest.Server {
	//serve dummy responses
	dummy_data := []byte(dummy_response)

	return createDummyServer(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != path {
			fmt.Printf("path: %v", r.URL.Path)
			t.Error("Path doesn't match")
		}
		w.Write(dummy_data)
	})
}

//test data
var dummyAccessTokenFacebookResponse string = "access_token=CAAIoGV3GFYQBAPqSZAgE85E1OkmPslPcba&expires=5183674"
var dummyAccessTokenResponse string = `
{
	"access_token": "46a54395f3d1108feca56c7f6ca8dd3d",
	"token_type": "bearer",
	"expires_in": 3600,
	"scope": "read",
	"user_id": "USERNAME"
} `

var dummyAccesTokenErrorResponse string = `
{
	"error": "invalid_request",
	"error_description": "Invalid grant_type parameter or parameter missing" 
} `

var dummyUserInfoFacebookResponse string = `
{
	"id":"100000304611015",
	"name":"John Bone",
	"first_name":"John",
	"last_name":"Bone",
	"link":"https:\/\/www.facebook.com\/john.bone",
	"gender":"male",
	"timezone":0,
	"locale":"en_GB",
	"verified":true,
	"updated_time":"2013-12-21T17:03:31+0000",
	"username":"john.bone"
} `
